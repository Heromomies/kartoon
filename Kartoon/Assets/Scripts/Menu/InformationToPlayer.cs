﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class InformationToPlayer : MonoBehaviour
{
    public GameObject[] panels;
    
    public void Player1()
    {
        Debug.Log("Player 1");
        foreach (var panel in panels)
        {
            panel.SetActive(false);
        }
        panels[0].SetActive(true);
    }
    public void Player2()
    {
        Debug.Log("Player 2");
        foreach (var panel in panels)
        {
            panel.SetActive(false);
        }
        panels[1].SetActive(true);
    }
    public void Player3()
    {
        Debug.Log("Player 3");
        foreach (var panel in panels)
        {
            panel.SetActive(false);
        }
        panels[2].SetActive(true);
    }
    public void Player4()
    {
        Debug.Log("Player 4");
        foreach (var panel in panels)
        {
            panel.SetActive(false);
        }
        panels[3].SetActive(true);
    }

    public void Buttons()
    {
        Debug.Log("Buttons");
        foreach (var panel in panels)
        {
            panel.SetActive(false);
        }
        panels[4].SetActive(true);
    }
}
